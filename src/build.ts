import { readFileSync } from 'fs';

import { ApiRunner } from './entities/apiRunner';

async function main()
{
    console.log('Building....');
    await new ApiRunner().build( JSON.parse(readFileSync('apis.json').toString()) );
}

main().then( e => process.exit(0) ).catch( e => console.log(e) );