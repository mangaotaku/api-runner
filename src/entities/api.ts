import Config from "../config";
import { IApi } from "../interfaces/iApi";

export class Api implements IApi {
    port: string;
    command: string;
    priority: number;
    branch: string;
    domain: string;
    git: string;
    name: string;
    env: { [key: string]: string };

    constructor( api?: Partial<IApi> )
    {
        this.port = api?.port;
        this.command = api?.command;
        this.priority = api?.priority;
        this.domain = api?.domain;
        this.branch = api?.branch || Config.MASTER_BRANCH;
        this.git = api?.git;
        this.name = api?.name;
        this.env = api?.env;
    }

    static fromJson(obj: any): IApi
    {
        if ( obj === undefined )
        {
            return undefined;
        }
        return new Api(obj);
    }
}