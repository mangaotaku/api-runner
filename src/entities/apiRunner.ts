import { exec, execSync } from 'child_process';

import Config from '../config';
import { IApi } from '../interfaces/iApi';
import { IApiRunner } from '../interfaces/iApiRunner';
import { Api } from './api';

export class ApiRunner {
    apis: IApi[];
    command: string;
    token: string;

    constructor( runner?: Partial<IApiRunner>, private reset: boolean = false )
    {
        this.apis = runner?.apis;
        this.command = runner?.command;
        this.token = runner?.token;
    }

    private async waitFor(msDelay?: number) {
        return await new Promise((resolve) => setTimeout(resolve, msDelay));
    }

    async build( runner: IApiRunner ): Promise<void>
    {
        const tmpDir = Config.TMP_DIRECTORY_NAME;
        this.apis = runner.apis;
        this.command = runner.command;

        try
        {
            execSync(`mkdir ${tmpDir}`);
        }
        catch(e)
        {
            if ( this.reset )
            {
                execSync(`rm -rf ${tmpDir}`);
                execSync(`mkdir ${tmpDir}`);
            }
        }
        let proc = [];
        for ( const api of this.apis )
        {
            try
            {
                execSync(`cd ${tmpDir} && git clone ${api.git}`);
            }
            catch(e)
            {
                execSync(`cd ${tmpDir}/${api.name} && git pull origin master`);
                
            }
            if ( api.branch )
            {
                execSync(`cd ${tmpDir}/${api.name} && git checkout ${api.branch} && git pull`)
            }
            let dir = __dirname.split('/');
            dir.pop();
            dir.pop();
            let x = exec(`cd ${[ tmpDir, api.name ].join('/')} && npm install`,
                { env: {...api.env, ...JSON.parse(JSON.stringify(process.env))}},
                ( error , stdout: string, stderr: string ) => {
                    console.log(error, stdout, stderr );
                } );
                proc.push(x);
        }
        do {
            if ( proc[0].exitCode === 0 )
            {
                proc.shift();
            }
            else
            {
                await this.waitFor(2000);
            }
        } while(proc.length > 0);
    }

    private async waitTilHealthy( apis: IApi[] ): Promise<void>
    {
        let domains = apis.filter( api => api.domain ).map( api => `${api.domain}:${api.port}`);
        do {
            const domain = domains[0];
            try
            {
                execSync(`curl http://${domain}/versions`);
                console.log(`Connected to ${domain}`);
                domains.shift();
            }catch{
                console.log(`waiting for ${domain}...`);
                await this.waitFor(2000);
            }
        }while( domains.length > 0)
    }

    async execute(): Promise<any>
    {
        const tmpDir = Config.TMP_DIRECTORY_NAME;
        for ( const api of this.apis.sort( (a,b) => ( a.priority === b.priority? 0: (a.priority > b.priority ? 1: -1) ) ) )
        {
            let dir = __dirname.split('/');
            dir.pop();
            dir.pop();
            exec(`cd ${[ tmpDir, api.name ].join('/')} && ${api.command}`,
                { env: {...api.env, ...JSON.parse(JSON.stringify(process.env))}},
                ( error , stdout: string, stderr: string ) => {
                    console.log(error, stdout, stderr );
                } );
        }
        await this.waitTilHealthy(this.apis);
        try
        {
            execSync(`cd tmp && ${this.command}`,{ stdio: "inherit" }).toString();
            return { status: 0 };
        }
        catch(e)
        {
            return e;
        }
    }

}