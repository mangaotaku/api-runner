export default class Config
{
    static readonly MASTER_BRANCH = 'master';
    static readonly TMP_DIRECTORY_NAME = process.env.TMP_DIRECTORY_NAME || 'tmp';
}