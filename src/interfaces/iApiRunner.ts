import { IApi } from "./iApi";

export interface IApiRunner {
    apis: IApi[];
    command: string;
    token: string;

    execute( runner: IApiRunner ): Promise<void>;
}