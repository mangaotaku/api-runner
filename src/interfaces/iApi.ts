export interface IApi {
    port: string;
    command: string;
    domain: string;
    branch: string;
    priority: number;
    git: string;
    name: string;
    env: { [key: string]: string };
}