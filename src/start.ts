import { readFileSync } from 'fs';

import { ApiRunner } from './entities/apiRunner';

async function main()
{
    console.log('Starting....');
    let apiRunner = new ApiRunner(JSON.parse(readFileSync('apis.json').toString()));
    process.exit((await apiRunner.execute()).status || 0);
}

main().catch( e => console.log(e) );